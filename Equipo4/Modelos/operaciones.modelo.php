<?php

   class Operaciones{
       private $numero1;
       private $numero2;
       public $respuesta2;

       public function getRespuesta2()
       {
           return $this->respuesta2;
       }

      

       public function getNumero1()
       {
           return $this->numero1;
       }

       public function setNumero1($numero1)
       {
           $this->numero1 = $numero1;
       }

       public function getNumero2()
       {
           return $this->numero2;
       }

       public function setNumero2($numero2)
       {
          $this->numero2 = $numero2;
       }

       public function punto2()
      {
        if ($this->numero1 > 0 && $this->numero2 > 0) 
        {
          if ($this->numero1 > $this->numero2) 
          {
            $respuesta = "El hermano numero 1 es mayor a hermano2"; 
            $dif = ($this->numero1-$this->numero2);
            $this->respuesta2 = "La diferencia de los hermanos es de ". $dif;
             
          }  
          elseif ($this->numero2 > $this->numero1) 
          {
            $respuesta = "El hermano numero 2 es mayor a hermano1"; 

               $this->respuesta2 = "La diferencia de los hermanos es de ".($this->numero2-$this->numero1);
          }
        }
        else 
        {
          $respuesta = "La edad de los dos hermanos es mayor a 0";
        }

        return  $respuesta;
      }

       public function punto4()
      {
        if ($this->numero1 >= 0 && $this->numero1 <10) 
        {
          if ($this->numero1 == 1) 
          {
            $romanos = "I";
          }
          if ($this->numero1 == 2) 
          {
            $romanos = "II";
          }
          if ($this->numero1 == 3) 
          {
            $romanos = "III";
          }
          if ($this->numero1 == 4) 
          {
            $romanos = "IV";
          }
          if ($this->numero1 == 5) 
          {
            $romanos = "V";
          }
          if ($this->numero1 == 6) 
          {
            $romanos ="VI";
          }
          if ($this->numero1 == 7) 
          {
            $romanos = "VII";
          }
          if ($this->numero1 == 8) 
          {
            $romanos = "VIII";
          }
          if ($this->numero1 == 9) 
          {
            $romanos = "IX";
          }
          if ($this->numero1 == 10) 
          {
            $romanos = "X";
          }
        }
        else 
        {
          $romanos = "El numero no esta entre 1 y 10";
        }

        return $romanos;
      }
   }
?>


