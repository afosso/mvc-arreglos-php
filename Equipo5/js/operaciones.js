function punto1(){
    if($('#horas').val() != ""){
        var parametros = {
            "numero1" : $('#horas').val(),
            "numero2" : $('#tarifa').val(),
            "accion" : "punto1"
        }

        $.ajax({
            data: parametros,
            url: '../controladores/operaciones.controlador.php',
            type: 'post',
            dataType: 'json',
            success: function(response){
               
                alert("El salario estipulado es de: " + response.valor);
            },
            error: function(response){
                alert(response);
            }
        });
    }
}

function punto2(){
    if($('#horas').val() != "" && $('#munitos').val() != ""){
        var parametros = {
            "numero1" : $('#horas').val(),
            "numero2" : $('#minutos').val(),
            "accion" : "punto2"
        }

        $.ajax({
            data: parametros,
            url: '../controladores/operaciones.controlador.php',
            type: 'post',
            dataType: 'json',
            success: function(response){
                alert("El resultado de la operación es de: " + response.valor);
            },
            error: function(response){
                alert(response);
            }
        });
    }
}


