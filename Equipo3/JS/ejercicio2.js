function ejercicio2()
{
    if($('#txtVocal').val() != ""){
        var parametros ={
            "vocal1" : $('#txtVocal').val(),
            "accion" : "ejercicio2"
        }

        $.ajax({
            data: parametros,
            url: '../Controlador/ejercicio2.controlador.php',
            type: 'post',
            dataType: 'json',
            success: function(response){
                alert("El resultado de la operación es de: " + response.valor);
            },
            error: function(response){
                alert(response);
            }
        });
    }
}