function ejercicio1()
{
    if($('#txtNumero').val() != ""){
        var parametros ={
            "numero1" : $('#txtNumero').val(),
            "accion" : "ejercicio1"
        }

        $.ajax({
            data: parametros,
            url: '../Controlador/ejercicio1.controlador.php',
            type: 'post',
            dataType: 'json',
            success: function(response){
                alert("El resultado de la operación es de: " + response.valor);
            },
            error: function(response){
                alert(response);
            }
        });
    }
}