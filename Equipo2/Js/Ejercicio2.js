function calcular(){
    if($('#txtedad').val() != "" && $('#txtnivel').val() != ""){
        
        var parametros = {
            "edad" : $('#txtedad').val(),
            "nivelHemo" : $('#txtnivel').val(),
            "sexo": $('input[name="sexo"]:checked').val(),
            "edadMA": $('input[name="edadMA"]:checked').val(),
        }

        $.ajax({
            data: parametros,
            url: '../controlador/Ejercicio2.controlador.php',
            type: 'post',
            dataType: 'json',
            success: function(response){
                alert("Anemia " + response.valor);
            },
            error: function(response){
                alert(response);
            }
        });
    }
}
