function HallarNumero(){
    var parametros = {
        "numero1" : $('#txtNum1').val(),
        "numero2" : $('#txtNum2').val(),
        "numero3" : $('#txtNum3').val()
    }

    $.ajax({
        data:parametros,
        url:'../Controlador/Ejercicio4.controlador.php',
        type: 'post',
        dataType: 'json',
        success: function(response){
            alert("El número de la mitad es: "+response.numMedio);
        },
        error: function(response){
            alert(response.responseText);
        }
    })
}