function calcular(){
    if($('#txtPromedio').val() != "" && $('#txtUnidades').val() != "" && $('#txtPromedio').val()<=10 && $('#txtPromedio').val()>=0){
        var parametros = {
            "promedio": $('#txtPromedio').val(),
            "unidades": $('#txtUnidades').val(),
            "materiasRepro": $('#txtMateriasRepro').val(),
            "tipoEstudiante": $('input[name="txtTipoEstudiante"]:checked').val()
        }

        $.ajax({
            data: parametros,
            url: '../Controlador/Ejercicio3.controlador.php',
            type: 'post',
            dataType: 'json', 
            success: function(response){
                alert("Cantidad máxima de unidades que puede cursar: " + response.respuesta1+ "\nCantidad de unidades con que se calculó la matrícula: "+ response.respuesta2+"\nDescuento aplicado: "+response.respuesta3+"%"+"\nTotal matrícula: "+response.respuesta4);
            },
            error: function(response){
                alert(response);
            }
        });
    
    }else{
        alert("Error. Verifique los valores ingresados.")
    }
}

