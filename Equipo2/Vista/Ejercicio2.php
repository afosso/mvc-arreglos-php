<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset= "UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ejercicio 2</title>
        <script src="../Js/jquery.js"></script>
        <script src="../Js/Ejercicio2.js"></script>
    </head>
    <body>
        <form>
            <p>Edad: <input type="number" name="txtedad" id="txtedad"> </p>
            <p><label><input type="radio" name="edadMA" value="meses"> meses </label> <label> <input type="radio" name="edadMA" value="años" checked> años</label></p>
            <p>Nivel Hemoglobina: <input type="number" name="txtnivel" id="txtnivel"> </p>
            <p>Sexo:</p>
            <p> Femenino:<input type="radio" name="sexo" id="sexoF" value="Femenino" checked></p>
            <p> Masculino:<input type="radio" name="sexo" id="sexoM" value="Masculino"></p>
            <button type="button" id="btnEnviar" onclick="calcular();">Calcular</button>
        </form>
    </body>
</html>