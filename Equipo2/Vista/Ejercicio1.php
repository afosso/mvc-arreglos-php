<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ejercicio1</title>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="../Js/Ejercicio1.js"></script>
</head>
<body>
    <div>
        <p><label for="txtSaldoActual">Saldo Actual: </label><br>
        <input type="number" name="txtSaldoActual" id="txtSaldoActual" placeholder="Por favor digite su saldo actual"></p>
    </div>
    <button id="btnEnviar" onclick="PedirPrestamo();">Enviar</button>
</body>
</html>