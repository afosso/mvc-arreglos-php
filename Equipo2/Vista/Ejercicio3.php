<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Page Title</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="../Js/jquery.js"></script>
        <script src="../Js/Ejercicio3.js"></script>
    </head>
    <body>
        <p>Promedio: <input type="number" name="txtPromedio" id="txtPromedio"> </p>
        <strong>Tipo alumno:</strong><br>
        <p>Preparatoria <input type="radio" name="txtTipoEstudiante" value="Preparatoria" checked></p>
        <p>Profesional <input type="radio" name="txtTipoEstudiante"  value="Profesional" ></p>
        <p>Unidades que desea matricular: <input type="number" name="txtUnidades" id="txtUnidades"></p>
        <p>Materias reprobadas: <input type="number" name="txtMateriasRepro" id="txtMateriasRepro"></p>
        <button type="button" id="calcular" onclick="calcular();">Calcular</button>
    </body>
</html>