<?php
    class Ejercicio3{
        private $promedio;
        private $tipoEstudiante;
        private $unidades;
        private $descuento;
        private $precioMatricula;
        private $costo;
        private $materiasRepro;
        private $unidadesMax;
        private $total;

        public function setPromedio($promedio){
            $this->promedio=$promedio;
        }
        public function setTipoEstudiante($tipoEstudiante){
            $this->tipoEstudiante=$tipoEstudiante;
        }
        public function setMateriasRepro($materiasRepro){
            $this->materiasRepro=$materiasRepro;
        }
        public function setUnidades($unidades){
            $this->unidades=$unidades;
        }
        public function getPromedio(){
            return $this->promedio;
        }
        public function getTipoEstudiante(){
            return $this->tipoEstudiante;
        }
        public function getUnidades(){
            return $this->unidades;
        }
        public function getMateriasRepro(){
            return $this->materiasRepro;
        }
        public function calcular(){
            if($this->tipoEstudiante=="Profesional"){
                $this->unidadesMax=55;
                $this->precioMatricula=300;
                if($this->promedio>=9.5){
                    $this->descuento=20;
                    if($this->unidades<=55){
                        $this->total= $this->calcularTotal();
                    }else{
                        $this->unidades=55;
                        $this->total=$this->calcularTotal();
                    }
                }else{
                    $this->descuento=0;
                    $this->total= $this->calcularTotal();
                }
            }else{
                $this->precioMatricula=180;
                if($this->promedio>=9.5){
                    $this->unidadesMax=55;
                    $this->descuento=25;
                    if ($this->unidades<=55) {
                        $this->total=$this->calcularTotal();
                    } else {
                        $this->unidades=55;
                        $this->total=$this->calcularTotal();
                    }
                    
                }else{
                    if($this->promedio>=9){
                        $this->descuento=10;
                        $this->unidadesMax=50;
                        if($this->unidades<=50){
                            
                            $this->total=$this->calcularTotal();
                        }else{
                            $this->unidades=50;
                            $this->total=$this->calcularTotal();
                        }
                    }else{
                        $this->descuento=0;
                        if($this->promedio>=7){
                            $this->unidadesMax=50;
                            if($this->unidades<=50){
                                
                                $this->total=$this->calcularTotal();
                            }else{
                                $this->unidades=50;
                                $this->total=$this->calcularTotal();
                            }
                        }else{
                            //materiasReprobadas
                            if($this->materiasRepro>=0 && $this->materiasRepro<=3){
                                $this->unidadesMax=45;
                                if($this->unidades<=45){
                                    $this->total=$this->calcularTotal();
                                }else{
                                    $this->unidades=45;
                                    $this->total=$this->calcularTotal(); 
                                }
                            }else{
                                $this->unidadesMax=40;
                                if($this->unidades<=40){
                                    $this->total=$this->calcularTotal();
                                }else{
                                    $this->unidades=40;
                                    $this->total=$this->calcularTotal(); 
                                }
                            }
                        }

                    }

                }

            }

            $respuesta = array(
                "respuesta1" => $this->unidadesMax,
                "respuesta2" => $this->unidades,
                "respuesta3" => $this->descuento,
                "respuesta4" => $this->total
            );
            return $respuesta;
        }
        public function calcularTotal(){
            
           return ($this->unidades*((100-$this->descuento)/100)*($this->precioMatricula))/5;
            
        }
    }


?>