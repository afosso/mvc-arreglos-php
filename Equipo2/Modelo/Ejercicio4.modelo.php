<?php
    
    class HallarNumero{
        private $num1;
        private $num2;
        private $num3;
        private $numMedio;

        public function setNum1($num1){
            $this->num1 = $num1;
        }

        public function setNum2($num2){
            $this->num2 = $num2;
        }

        public function setNum3($num3){
            $this->num3 = $num3;
        }

        public function getNumMedio(){
            return $this->numMedio;
        }

        public function hallarNumMedio(){
            if (($this->num1 > $this->num2 && $this->num2 > $this->num3) || ($this->num3 > $this->num2 && $this->num2 > $this->num1)){
                $this->numMedio = $this->num2;
            }else if(($this->num2 > $this->num1 && $this->num1 > $this->num3) || ($this->num3 > $this->num1 && $this->num1 > $this->num2)) {
                $this->numMedio = $this->num1;
            }else if(($this->num1 > $this->num3 && $this->num3 > $this->num2) || ($this->num2 > $this->num3 && $this->num3 > $this->num1)) {
                $this->numMedio = $this->num3;
            }else {
                $this->numMedio="Dos numeros iguales";
            }
            return $this->numMedio;
        }
    }
?>