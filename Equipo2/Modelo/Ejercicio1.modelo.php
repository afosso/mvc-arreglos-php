<?php

    class PedirPrestamo{
        private $saldoActual;
        private $saldoPrestamo;
        private $saldoNuevo;
        private $saldoComputo;
        private $saldoMobilario;
        private $saldoInsumos;
        private $resto;

        public function setSaldoActual($saldoActual){
            $this->saldoActual = $saldoActual;
        }

        public function getSaldoActual(){
            return $this->saldoActual;
        }

        public function getSaldoNuevo(){
            return $this->saldoNuevo;
        }

        public function getSaldoPrestamo(){
            return $this->saldoPrestamo;
        }
        
        public function getSaldoComputo(){
            return $this->saldoComputo;
        }
        
        public function getsaldoMobilario(){
            return $this->saldoMobilario;
        }

        public function getsaldoInsumos(){
            return $this->saldoInsumos;
        }

        public function hacerPrestamo(){
            if ($this->saldoActual<0){
                $this->saldoNuevo = 10000;
                $this->saldoPrestamo =  $this->saldoNuevo - $this->saldoActual;
            } elseif ($this->saldoActual>=0 && $this->saldoActual<20000) {
                $this->saldoNuevo = 20000;
                $this->saldoPrestamo =  $this->saldoNuevo -  $this->saldoActual;
            } else {
                $this->saldoNuevo =  $this->saldoActual;
                $this->saldoPrestamo = 0;
            }
        }

        public function repartirSaldo(){
            $this->saldoComputo = 5000;
            $this->saldoMobilario = 2000;
            $this->resto =  $this->saldoNuevo - 7000;
            $this->saldoInsumos =  $this->resto / 2;
        }
    }
?>