<?php

    class Ejercicio2{
        private $edad;
        private $nivelHemo;
        private $sexo;
        private $edadMA;
        private $nuevaEdad;
        private $caso;

        public function setEdad($edad){
            $this->edad=$edad;
        }
        public function setNivelHemo($nivelHemo){
            $this->nivelHemo=$nivelHemo;
        }

        public function getEdad(){
            return $this->edad;
        }

        public function getNivelHemo(){
            return $this->nivelHemo;
        }

        public function setSexo($sexo){
            $this->sexo=$sexo;
        }
        public function setEdadMA($edadMA){
            $this->edadMA=$edadMA;
        }
        public function getSexo(){
            return $this->sexo;
        }

        public function getEdadMA(){
            return $this->edadMA;
        }

        public function calcular(){

            if($this->edadMA=="meses"){
                if($this->edad<12){
                    if($this->edad<=1 && $this->edad>=0){
                        $this->caso=1;
                    }else{
                        if($this->edad>1 && $this->edad<=6){
                            $this->caso=2;
                        }else{
                            $this->caso=3;
                        }
                    }
                }else{
                    $this->nuevaEdad= $this->edad/12;
                    if($this->nuevaEdad>1 && $this->nuevaEdad<=5){
                        $this->caso=4;
                    }else{
                        if($this->nuevaEdad>5 && $this->nuevaEdad<=10){
                            $this->caso=5;
                        }else{
                            if($this ->nuevaEdad>10 && $this ->nuevaEdad<=15){
                                $this ->caso=6;
                            }else{
                                if($this ->sexo=="Masculino"){
                                    $this ->caso=8;
                                }else{
                                    $this ->caso=7;
                                }
                            }
                        }
                    }
                }
            }else{
                if($this ->edad<=15 && $this ->edad>=1){
                    if($this ->edad<=5 && $this ->edad>1){
                        $this ->caso=4;
                    }else{
                        if($this ->edad<=10 && $this ->edad>5){
                            $this ->caso=5;
                        }else{
                            $this ->caso=6;
                        }

                    }
                }else{
                    if($this ->sexo=="Masculino"){
                        $this ->caso=8;
                    }else{
                        $this ->caso=7;
                    }
                }
            }

            switch ($this ->caso) {
                case 1:
                    if($this-> nivelHemo>=13 && $this-> nivelHemo<=26){
                       return "Negativo";
                    }else{
                        return "Positivo";
                    }
                    break;
                case 2:
                    if($this-> nivelHemo>=10 && $this-> nivelHemo<=18){
                        return "Negativo";
                    }else{
                       return "Positivo";
                    }
                    break;
                case 3:
                    if($this-> nivelHemo>=11 && $this-> nivelHemo<=15){
                            return "Negativo";
                        }else{
                            return "Positivo";
                        }
                    break;
                case 4:
                    if($this-> nivelHemo>=11.5 && $this-> nivelHemo<=15){
                        return "Negativo";
                    }else{
                        return "Positivo";
                    }
                    break;
                case 5:
                    if($this-> nivelHemo>=12.6 && $this-> nivelHemo<=15.5){
                        return "Negativo";
                    }else{
                        return "Positivo";
                    }
                    break;
                case 6:
                    if($this-> nivelHemo>=13 && $this-> nivelHemo<=15.5){
                        return "Negativo";
                    }else{
                        return "Positivo";
                    }
                    break;
                case 7:
                    if($this-> nivelHemo>=12 && $this-> nivelHemo<=16){
                        return "Negativo";
                    }else{
                        return "Positivo";
                    }
                    break;
                case 8:
                    if($this-> nivelHemo>=14 && $this-> nivelHemo<=18){
                        return "Negativo";
                    }else{
                        return "Positivo";
                    }
                    break;
                default:
                    return "ERROR";
                    break;
                
            }
        }
    }

?>