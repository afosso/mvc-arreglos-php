<?php

    require_once '../Modelo/Ejercicio2.modelo.php';

    $respuesta = new Ejercicio2();
    $respuesta->setNivelHemo($_POST["nivelHemo"]);
    $respuesta->setEdad($_POST["edad"]);
    $respuesta->setSexo($_POST["sexo"]);
    $respuesta->setEdadMA($_POST["edadMA"]);
    
    $valor = array(

        "valor" => $respuesta->calcular()
        
    );
    
    echo json_encode($valor);


?>