<?php
    require_once '../Modelo/Ejercicio4.modelo.php';

    $respuesta = new HallarNumero();
    $respuesta->setNum1($_POST["numero1"]);
    $respuesta->setNum2($_POST["numero2"]);
    $respuesta->setNum3($_POST["numero3"]);

    $numMedio = array(
        "numMedio" => $respuesta->hallarNumMedio()
    );
    
    echo json_encode($numMedio);
    
?>