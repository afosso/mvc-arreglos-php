<?php

    require_once '../Modelo/Ejercicio1.modelo.php';

    $respuesta = new PedirPrestamo();
    $respuesta->setSaldoActual($_POST["saldoActual"]);
    $respuesta->hacerPrestamo();
    $respuesta->repartirSaldo();

    $saldos = array(
        //"saldoNuevo" => $respuesta->getSaldoNuevo(),
        "saldoPrestamo" => $respuesta->getSaldoPrestamo(),
        "saldoComputo" => $respuesta->getSaldoComputo(),
        "saldoMobilario" => $respuesta->getSaldoMobilario(),
        "saldoInsumos" => $respuesta->getSaldoInsumos()
    );
    
    echo json_encode($saldos);


?>